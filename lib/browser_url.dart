import 'dart:html' as html;

class BrowserUrl {
  static get url => html.window.location.href;
}
